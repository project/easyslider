

Description
-----------
This module integrates the jQuery EasySlider plugin with your list views..


Dependencies
------------
* jQuery Update (http://drupal.org/project/jquery_update)


Installation
------------
1) Place this module directory in your modules folder (this will usually be
"sites/all/modules/").

2) Enable the module.

3) Make a view and set the type to List, you will need to add fields, at east add node title and node body.



Author
------
Nicolas Borda

* mail: wnic@ipwa.net
* website: http://nic.ipwa.net

The author can be contacted for paid customizations of this module as well as Drupal consulting, development and installation.
